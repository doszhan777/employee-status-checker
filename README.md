# Employee Status Checker

## Instructions

Start the app:
```sh
docker-compose up --build
```

Location of the tests (3 successful and 2 failed cases as described in the task):
[EmployeeStatusChangeServiceTest.java](src/test/java/de/doszhan/states/service/EmployeeStatusChangeServiceTest.java)

Postman collection for testing:
[Employee.postman_collection.json](postman/Employee.postman_collection.json)

Screenshots of Postman:

![create](postman/create.png)
![all](postman/all.png)
![status](postman/status.png)

Have a nice day.