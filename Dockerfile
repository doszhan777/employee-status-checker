FROM openjdk:18-jdk
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} states-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/states-0.0.1-SNAPSHOT.jar"]
