package de.doszhan.states.repository;

import de.doszhan.states.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, java.util.UUID>  {
}