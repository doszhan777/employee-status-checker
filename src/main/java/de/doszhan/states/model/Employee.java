package de.doszhan.states.model;

import de.doszhan.states.state.EmployeeMainState;
import de.doszhan.states.state.EmployeeMainEvent;
import de.doszhan.states.state.EmployeePermitState;
import de.doszhan.states.state.EmployeePermitEvent;
import de.doszhan.states.state.EmployeeSecurityState;
import de.doszhan.states.state.EmployeeSecurityEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;
import java.util.Date;
import java.util.UUID;
import javax.persistence.*;

@Entity
@Getter
@Setter
public class Employee {
  @Id
  @Column(name = "id", columnDefinition = "uuid")
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  private UUID id;

  @Column(name = "fullname")
  private String fullname;

  private EmployeeMainState mainState = EmployeeMainState.ADDED;
  private EmployeeSecurityState securityState;
  private EmployeePermitState permitState;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created")
  private Date created;
}
