package de.doszhan.states.controller;

import de.doszhan.states.service.EmployeeService;
import de.doszhan.states.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

  private final EmployeeService employeeService;

  public EmployeeController(EmployeeService employeeService) {
    log.info("EmployeeController");
    this.employeeService = employeeService;
  }

  @PostMapping("/create")
  @ResponseStatus(HttpStatus.CREATED)
  public Employee create(String fullname) {
    return this.employeeService.create(fullname);
  }

  @PostMapping("/status/{employeeId}")
  public Employee event(@PathVariable("employeeId") UUID employeeId, String event) {
    return this.employeeService.setEventForEmployee(employeeId, event);
  }

  @GetMapping("/all")
  public List<Employee> all() {
    return this.employeeService.all();
  }
 
}
