package de.doszhan.states.service;

import de.doszhan.states.model.Employee;
import de.doszhan.states.state.EmployeeMainState;
import de.doszhan.states.state.EmployeeMainEvent;
import de.doszhan.states.state.EmployeePermitState;
import de.doszhan.states.state.EmployeePermitEvent;
import de.doszhan.states.state.EmployeeSecurityState;
import de.doszhan.states.state.EmployeeSecurityEvent;
import de.doszhan.states.repository.EmployeeRepository;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component
public class EmployeeStatusChangeService {

  private EmployeeSecurityState performSecurityEventAndReturnNewStatus(Employee employee, String event) {
    EmployeeSecurityState securityState = employee.getSecurityState();
    EmployeeSecurityEvent securityEvent = null;
    if (EnumUtils.isValidEnum(EmployeeSecurityEvent.class, event)) {
      securityEvent = EmployeeSecurityEvent.valueOf(event);
    }
    switch (securityState) {
      case SECURITY_CHECK_STARTED:
        if (securityEvent == EmployeeSecurityEvent.FINISH_SECURITY_CHECK) {
          employee.setSecurityState(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
        }
        break;
      default:
        break;
    }

    return employee.getSecurityState();
  }

  private EmployeePermitState performPermitEventAndReturnNewStatus(Employee employee, String event) {
    EmployeePermitState permitState = employee.getPermitState();
    EmployeePermitEvent permitEvent = null;
    if (EnumUtils.isValidEnum(EmployeePermitEvent.class, event)) {
      permitEvent = EmployeePermitEvent.valueOf(event);
    }
    switch (permitState) {
      case WORK_PERMIT_CHECK_STARTED:
        if (permitEvent == EmployeePermitEvent.COMPLETE_INITIAL_WORK_PERMIT_CHECK) {
          employee.setPermitState(EmployeePermitState.WORK_PERMIT_CHECK_PENDING_VERIFICATION);
        }
        break;
      case WORK_PERMIT_CHECK_PENDING_VERIFICATION:
        if (permitEvent == EmployeePermitEvent.FINISH_WORK_PERMIT_CHECK) {
          employee.setPermitState(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);
        }
        break;
      default:
        break;
    }

    return employee.getPermitState();
  }

  public Employee performMainEvent(Employee employee, String event) {
    EmployeeMainState mainState = employee.getMainState();
    EmployeeMainEvent mainEvent = null;
    if (EnumUtils.isValidEnum(EmployeeMainEvent.class, event)) {
      mainEvent = EmployeeMainEvent.valueOf(event);
    }
    switch (mainState) {
      case ADDED:
        if (mainEvent == EmployeeMainEvent.BEGIN_CHECK) {
          employee.setMainState(EmployeeMainState.IN_CHECK);
          employee.setSecurityState(EmployeeSecurityState.SECURITY_CHECK_STARTED);
          employee.setPermitState(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);
        }
        break;
      case IN_CHECK:
        EmployeeSecurityState securityState = performSecurityEventAndReturnNewStatus(employee, event);
        EmployeePermitState permitState = performPermitEventAndReturnNewStatus(employee, event);
        if (
          securityState == EmployeeSecurityState.SECURITY_CHECK_FINISHED
          && permitState == EmployeePermitState.WORK_PERMIT_CHECK_FINISHED
        ) {
          employee.setMainState(EmployeeMainState.APPROVED);
        }
        break;
      case APPROVED:
        if (mainEvent == EmployeeMainEvent.ACTIVATE) {
          employee.setMainState(EmployeeMainState.ACTIVE);
        }
        break;
      default:
        break;
    }
    
    return employee;
  }


}
