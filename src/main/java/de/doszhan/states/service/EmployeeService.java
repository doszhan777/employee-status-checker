package de.doszhan.states.service;

import de.doszhan.states.model.Employee;
import de.doszhan.states.state.EmployeeMainState;
import de.doszhan.states.state.EmployeeMainEvent;
import de.doszhan.states.state.EmployeePermitState;
import de.doszhan.states.state.EmployeePermitEvent;
import de.doszhan.states.state.EmployeeSecurityState;
import de.doszhan.states.state.EmployeeSecurityEvent;
import de.doszhan.states.repository.EmployeeRepository;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component
public class EmployeeService {

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private EmployeeStatusChangeService employeeStatusChangeService;
  
  public Employee create(String fullname) {
    Employee employee = new Employee();
    employee.setFullname(fullname);
    
    return employeeRepository.save(employee);
  }

  public Employee setEventForEmployee(UUID employeeId, String event) {
    Employee employee = employeeRepository.findById(employeeId).orElse(null);
    employee = employeeStatusChangeService.performMainEvent(employee, event);
    employeeRepository.save(employee);
    
    return employee;
  }

  public List<Employee> all() {
    return employeeRepository.findAll();
  }


}
