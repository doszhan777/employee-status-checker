package de.doszhan.states.state;

public enum EmployeeSecurityState {
    SECURITY_CHECK_STARTED,
    SECURITY_CHECK_FINISHED
}
