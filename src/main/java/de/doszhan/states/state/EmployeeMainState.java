package de.doszhan.states.state;

public enum EmployeeMainState {
    ADDED,
    IN_CHECK,
    APPROVED,
    ACTIVE
}
