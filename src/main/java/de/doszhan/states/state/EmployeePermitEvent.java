package de.doszhan.states.state;

public enum EmployeePermitEvent {
    COMPLETE_INITIAL_WORK_PERMIT_CHECK,
    FINISH_WORK_PERMIT_CHECK
}
