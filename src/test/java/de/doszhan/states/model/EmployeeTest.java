package de.doszhan.states.model;

import de.doszhan.states.state.EmployeeMainState;
import de.doszhan.states.state.EmployeeMainEvent;
import de.doszhan.states.state.EmployeePermitState;
import de.doszhan.states.state.EmployeePermitEvent;
import de.doszhan.states.state.EmployeeSecurityState;
import de.doszhan.states.state.EmployeeSecurityEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.junit.jupiter.api.Test;
import lombok.Getter;
import lombok.Setter;
import lombok.NonNull;
import java.util.Date;
import java.util.UUID;
import javax.persistence.*;

import static org.assertj.core.api.Assertions.assertThat;

public class EmployeeTest {
  
  @Test
  public void shouldContainNullStatesWhenInit() {
    Employee empl = new Employee();
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.ADDED);
    assertThat(empl.getSecurityState()).isEqualTo(null);
    assertThat(empl.getPermitState()).isEqualTo(null);
  }
}
