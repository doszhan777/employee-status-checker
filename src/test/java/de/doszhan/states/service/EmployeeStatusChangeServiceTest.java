package de.doszhan.states.service;

import de.doszhan.states.model.Employee;
import de.doszhan.states.state.EmployeeMainState;
import de.doszhan.states.state.EmployeeMainEvent;
import de.doszhan.states.state.EmployeePermitState;
import de.doszhan.states.state.EmployeePermitEvent;
import de.doszhan.states.state.EmployeeSecurityState;
import de.doszhan.states.state.EmployeeSecurityEvent;
import de.doszhan.states.repository.EmployeeRepository;
import org.apache.commons.lang3.EnumUtils;
import org.mockito.InjectMocks;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.jupiter.api.Test;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Component
public class EmployeeStatusChangeServiceTest {

  @InjectMocks
  private EmployeeStatusChangeService employeeStatusChangeService = new EmployeeStatusChangeService();

  @Test
  public void shouldSucceedByRunningSecurityFirst() {
    Employee empl = new Employee();
    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.BEGIN_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeSecurityEvent.FINISH_SECURITY_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.COMPLETE_INITIAL_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_PENDING_VERIFICATION);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.FINISH_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.APPROVED);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.ACTIVATE.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.ACTIVE);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);
  }

  @Test
  public void shouldSucceedByRunningPermitFirst() {
    Employee empl = new Employee();
    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.BEGIN_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.COMPLETE_INITIAL_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_PENDING_VERIFICATION);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.FINISH_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeSecurityEvent.FINISH_SECURITY_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.APPROVED);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.ACTIVATE.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.ACTIVE);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);
  }

  @Test
  public void shouldSucceedByRunningWorkSecurityWork() {
    Employee empl = new Employee();
    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.BEGIN_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.COMPLETE_INITIAL_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_PENDING_VERIFICATION);

    employeeStatusChangeService.performMainEvent(empl, EmployeeSecurityEvent.FINISH_SECURITY_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_PENDING_VERIFICATION);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.FINISH_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.APPROVED);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.ACTIVATE.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.ACTIVE);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_FINISHED);
  }

  @Test
  public void shouldNotSucceedByActivatingWithoutWorkPermit() {
    Employee empl = new Employee();
    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.BEGIN_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeSecurityEvent.FINISH_SECURITY_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.ACTIVATE.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);
  }

  @Test
  public void shouldNotSucceedByFinishingPermitWithoutValidating() {
    Employee empl = new Employee();
    employeeStatusChangeService.performMainEvent(empl, EmployeeMainEvent.BEGIN_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_STARTED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeeSecurityEvent.FINISH_SECURITY_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);

    employeeStatusChangeService.performMainEvent(empl, EmployeePermitEvent.FINISH_WORK_PERMIT_CHECK.name());
    assertThat(empl.getMainState()).isEqualTo(EmployeeMainState.IN_CHECK);
    assertThat(empl.getSecurityState()).isEqualTo(EmployeeSecurityState.SECURITY_CHECK_FINISHED);
    assertThat(empl.getPermitState()).isEqualTo(EmployeePermitState.WORK_PERMIT_CHECK_STARTED);
  }

}
